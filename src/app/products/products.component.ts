import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';

import { ProductService } from '../product.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Product } from '../models/product';
import { ShoppingCartService } from '../shopping-cart.service';
import { ShoppingCart } from '../models/shopping-cart';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  
  category: string;
  products: Product[] = [];
  filteredProducts: Product[] = [];
  cart$: Observable<ShoppingCart>;
  
  constructor(
    private shoppingCartService: ShoppingCartService,
    private route: ActivatedRoute,
    private productService: ProductService) {}
    
    async ngOnInit() {
      this.cart$ = await this.shoppingCartService.getCart();
      
      this.populateProducts();
    }
    
    private populateProducts() {
      this.productService.getAll()
      .switchMap((products: Product[]) => {
        this.products = products;
        
        return this.route.queryParamMap;
      })
      .subscribe(params => {
        this.category = params.get('category');
        
        this.applyFilter();
      });
    }
    
    private applyFilter() {
      this.filteredProducts = (this.category) ?
      this.products.filter(p => p.category === this.category) :
      this.products;
    }
    
  }
  