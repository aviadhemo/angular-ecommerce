import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ProductService {
  readonly path = '/products';

  constructor(private db: AngularFireDatabase) { }

  create(product) {
    return this.db.list(this.path).push(product);
  }

  getAll() {
    return this.db.list(this.path);
  }

  get(productId) {
    return this.db.object(`${this.path}/${productId}`);
  }

  update(productId, product) {
    return this.db.object(`${this.path}/${productId}`).update(product);
  }

  delete(productId) {
    return this.db.object(`${this.path}/${productId}`).remove();
  }
}
