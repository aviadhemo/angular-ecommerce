import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';

import { Product } from './models/product';
import { ShoppingCart } from './models/shopping-cart';

@Injectable()
export class ShoppingCartService {
  readonly path = '/shopping-carts';
  
  constructor(private db: AngularFireDatabase) { }
  
  async getCart(): Promise<Observable<ShoppingCart>> {
    const cartId = await this.getOrCreateCartId();
    return this.db.object(`${this.path}/${cartId}`)
    .map(x => new ShoppingCart(x.items));
  }
  
  async addToCart(product: Product) {
    this.updateItem(product, 1);
  }
  
  async removeFromCart(product: Product) {
    this.updateItem(product, -1);
  }
  
  async clearCart() {
    const id = await this.getOrCreateCartId();
    this.db.object(`/shopping-carts/${id}/items`).remove();
  }
  
  private create() {
    return this.db.list(this.path).push({
      dateCreated: new Date().getTime()
    });
  }
  
  private async updateItem(product: Product, change: number) {
    const cartId = await this.getOrCreateCartId();
    const item$ = this.getItem(cartId, product.$key);
    
    item$.take(1).subscribe(item => {
      const quantity = (item.quantity || 0) + change;
      
      if(quantity === 0) {
        item$.remove();
      } else {
        item$.update({ 
          title: product.title,
          imageUrl: product.imageUrl,
          price: product.price,
          quantity
        });
      }
    });
  }
  
  private async getOrCreateCartId(): Promise<string> {
    let cartId = localStorage.getItem('cartId');
    
    if(!cartId) {
      const result = await this.create();
      localStorage.setItem('cartId', result.key);
      cartId = result.key;
    }
    
    return cartId;
  }
  
  private getItem(cartId: string, productId: string) {
    return this.db.object(`${this.path}/${cartId}/items/${productId}`);
  }
}
