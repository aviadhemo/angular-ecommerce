# AngularEcommerce

[link to project sample in firebase](https://ng-oshop.firebaseapp.com/)

#### project structure
```
├── src
   ├── app
      ├── admin
      │   ├── admin-orders
      │   │   ├── admin-orders.component.css
      │   │   ├── admin-orders.component.html
      │   │   └── admin-orders.component.ts
      │   └── admin-products
      │       ├── admin-products.component.css
      │       ├── admin-products.component.html
      │       └── admin-products.component.ts
      ├── admin-auth-guard.service.ts
      ├── app.component.css
      ├── app.component.html
      ├── app.component.spec.ts
      ├── app.component.ts
      ├── app.module.ts
      ├── auth-guard.service.spec.ts
      ├── auth-guard.service.ts
      ├── auth.service.spec.ts
      ├── auth.service.ts
      ├── bs-navbar
      │   ├── bs-navbar.component.css
      │   ├── bs-navbar.component.html
      │   └── bs-navbar.component.ts
      ├── category.service.spec.ts
      ├── category.service.ts
      ├── check-out
      │   ├── check-out.component.css
      │   ├── check-out.component.html
      │   └── check-out.component.ts
      ├── home
      │   ├── home.component.css
      │   ├── home.component.html
      │   └── home.component.ts
      ├── login
      │   ├── login.component.css
      │   ├── login.component.html
      │   └── login.component.ts
      ├── models
      │   ├── app-user.ts
      │   └── product.ts
      ├── my-orders
      │   ├── my-orders.component.css
      │   ├── my-orders.component.html
      │   └── my-orders.component.ts
      ├── order-success
      │   ├── order-success.component.css
      │   ├── order-success.component.html
      │   └── order-success.component.ts
      ├── product-card
      │   ├── product-card.component.css
      │   ├── product-card.component.html
      │   └── product-card.component.ts
      ├── product-form
      │   ├── product-form.component.css
      │   ├── product-form.component.html
      │   └── product-form.component.ts
      ├── products
      │   ├── product-filter
      │   │   ├── product-filter.component.css
      │   │   ├── product-filter.component.html
      │   │   └── product-filter.component.ts
      │   ├── products.component.css
      │   ├── products.component.html
      │   └── products.component.ts
      ├── product.service.spec.ts
      ├── product.service.ts
      ├── shopping-cart
      │   ├── shopping-cart.component.css
      │   ├── shopping-cart.component.html
      │   └── shopping-cart.component.ts
      ├── shopping-cart.service.ts
      ├── user.service.spec.ts
      └── user.service.ts
   


```

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.0.

